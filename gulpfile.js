
// folders
const projectFolder = "app";
const sourceFolder = "src";

// pathes
const path = {
    app: {
        html: projectFolder + "/",
        css: projectFolder + "/css/",
        js: projectFolder + "/js/",
        img: projectFolder + "/img/",
        fonts: projectFolder + "/fonts/"
    },
    src: {
        html: sourceFolder + "/index.pug",
        css: sourceFolder + "/scss/style.scss",
        js: sourceFolder + "/js/script.js",
        img: sourceFolder + "/img/**/*.{png,jpg,svg}",
        fonts: sourceFolder + "/fonts/*.ttf"
    },
    clean: "./" + projectFolder + "/"
}

// plugins
let gulp = require("gulp"),
    browserSync = require("browser-sync").create(),
    pug = require("gulp-pug"),
    sass = require("gulp-sass")(require("sass")),
    autoprefixer = require("gulp-autoprefixer"),
    del = require("del");

// Tasks for plugins params
function clean() {
    return del(path.clean);
}

function browser_sync() {
    browserSync.init({
        server: {
            baseDir: projectFolder
        },
        port: 3000,
        notify: false,
    });
}

function html() {
    return gulp.src(path.src.html)
          .pipe(pug({
              outputStyle: "expanded"
          }))
          .pipe(gulp.dest(path.app.html))
          .pipe(browserSync.stream());
}

function watchFiles() {
    gulp.watch([path.src.html], html);
    gulp.watch([path.src.css], css);
    gulp.watch([path.src.img], images);
}

function css() {
    return gulp.src(path.src.css)
          .pipe(sass({
              outputStyle: "expanded"
          }))
          .pipe(autoprefixer({
              overrideBrowsersList: ["last 5 versions"],
              cascade: true
          }))
          .pipe(gulp.dest(path.app.css))
          .pipe(browserSync.stream());
}

function images() {
    return gulp.src(path.src.img)
          .pipe(gulp.dest(path.app.img))
          .pipe(browserSync.stream());
}


let build = gulp.parallel(html, css, images);
let watch = gulp.parallel(build, watchFiles, browser_sync);

exports.images = images;
exports.css = css;
exports.html = html;
exports.build = build;
exports.watch = watch;
exports.default = watch;